<?php 

require 'index1.php';

class Frog extends Animal{
    public
        $legs = 4;
    
    public function __construct($name = "name")
    {
        parent::__construct($name);
    }

    public function jump(){
        return "Hop hop";
    }
}

$kodok = new Frog("buduk");
echo "Kodok ini bernama: " . $kodok->name;
echo "<br>";
echo "Kodok merupakan hewan berkaki: " . $kodok->legs;
echo "<br>";
echo $kodok->jump() ; // "hop hop"

?>
