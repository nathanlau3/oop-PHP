<?php 

require 'index1.php';

class Ape extends Animal{
    public function __construct($name = "name")
    {
        parent::__construct($name);
    }

    public function yell(){
        return "Auooo";
    }
}

$sungokong = new Ape("kera sakti");
echo "Sungokong ini bernama: " . $sungokong->name;
echo "<br>";
echo "Sungokong merupakan hewan berkaki: " . $sungokong->legs;
echo "<br>";
echo $sungokong->yell() ; // "hop hop"

?>
